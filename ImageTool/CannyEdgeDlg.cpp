﻿// CannyEdge.cpp: 구현 파일
//

#include "stdafx.h"
#include "ImageTool.h"
#include "CannyEdgeDlg.h"
#include "afxdialogex.h"


// CCannyEdge 대화 상자

IMPLEMENT_DYNAMIC(CCannyEdgeDlg, CDialogEx)

CCannyEdgeDlg::CCannyEdgeDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_CANNY_EDGE, pParent)
    , m_fSigma(1.4)
    , m_fLowTh(30.0)
    , m_fHighTh(60.0)
{

}

CCannyEdgeDlg::~CCannyEdgeDlg()
{
}

void CCannyEdgeDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialogEx::DoDataExchange(pDX);
    DDX_Text(pDX, IDC_CANNY_SIGMA, m_fSigma);
    DDX_Text(pDX, IDC_LOW_THRESHOLD, m_fLowTh);
    DDX_Text(pDX, IDC_HIGH_THRESHOLD, m_fHighTh);
}


BEGIN_MESSAGE_MAP(CCannyEdgeDlg, CDialogEx)
END_MESSAGE_MAP()


// CCannyEdge 메시지 처리기
