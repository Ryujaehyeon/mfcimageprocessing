#pragma once
#include "IppImage.h"

void IppInverse        (IppRgbImage& img);
void RGB_TO_HSI        (double r, double g, double b, double& h, double &s, double& i);
void HSI_TO_RGB        (double h, double s, double i, double& r, double& g, double& b);
void RGB_TO_YUV        (BYTE r, BYTE g, BYTE b, BYTE& y, BYTE& u, BYTE& v);
void YUV_TO_RGB        (BYTE y, BYTE u, BYTE v, BYTE& r, BYTE& g, BYTE& b);
void IppColorSplitRGB  (IppRgbImage& imgColor, IppByteImage& imgR, IppByteImage& imgG, IppByteImage& imgB);
void IppColorSplitHSI  (IppRgbImage& imgColor, IppByteImage& imgH, IppByteImage& imgS, IppByteImage& imgI);
void IppColorSplitYUV  (IppRgbImage& imgColor, IppByteImage& imgY, IppByteImage& imgU, IppByteImage& imgV);
bool IppColorCombineRGB(IppByteImage& imgR, IppByteImage& imgG, IppByteImage& imgB, IppRgbImage& imgColor);
bool IppColorCombineHSI(IppByteImage& imgH, IppByteImage& imgS, IppByteImage& imgI, IppRgbImage& imgColor);
bool IppColorCombineYUV(IppByteImage& imgY, IppByteImage& imgU, IppByteImage& imgV, IppRgbImage& imgColor);
void IppColorEdge      (IppRgbImage & imgSrc, IppByteImage & imgEdge);
