#include "stdafx.h"
#include "IppAvi.h"
#include "RGBBYTE.h"

IppAvi::IppAvi()
{
    // AVIfile 라이브러리를 초기화 한다.
    AVIFileInit();

    // 멤버 초기화
    m_pAviFile = nullptr;
    m_pVideoStream = nullptr;
    m_pVideoFrame = nullptr;

    m_nWidth = 0;
    m_nHeight = 0;

    m_nTotalFrame = 0;
    m_nFrameRate = 0;
}


IppAvi::~IppAvi()
{
    Close();

    // AVIfile 라이브러리 사용을 종료한다.
    AVIFileExit();
}

BOOL IppAvi::Open(LPCTSTR lpszPathName)
{
    int i;
    HRESULT hr;

    AVIFILEINFO aviFileInfo;
    AVISTREAMINFO aviStreamInfo;
    PAVISTREAM pAviTmpStream = nullptr;

    // 기존에 파일을 Open 했었으면 닫는다.
    if (m_pAviFile)
    {
        Close();
        m_pAviFile = nullptr;
    }

    // avi 파일을 열고, 파일 인터페이스에 대한 포인터를 m_pAviFile에 저장한다.
    hr = AVIFileOpen(&m_pAviFile, lpszPathName, OF_READ, nullptr);
    if (hr)
        return FALSE;

    // AVI 파일에 대한 정보를 얻는다.
    hr = AVIFileInfo(m_pAviFile, &aviFileInfo, sizeof(AVIFILEINFO));
    if (hr)
    {
        Close();
        return FALSE;
    }

    for (i = 0; i < (LONG)aviFileInfo.dwStreams; i++)
    {
        // AVI 파일과 관련된 스트림 인터페이스의 주소를 얻는다.
        // fccType이 0L으로 지정되면 모든 스트림을 검색한다.

        hr = AVIFileGetStream(m_pAviFile, &pAviTmpStream, 0L, i);
        
        if (hr)
            continue;

        // 위에서 받은 스트림 인터페이스에 대한 정보를 얻는다.
        hr = AVIStreamInfo(pAviTmpStream, &aviStreamInfo, sizeof(AVISTREAMINFO));

        if (hr)
            continue;

        if (aviStreamInfo.fccType == streamtypeVIDEO)
        {
            m_pVideoStream = pAviTmpStream;

            // AVI 스트림의 사용 참조 개수를 증가시킨다.
            AVIStreamAddRef(m_pVideoStream);

            // 전체 샘플 수 (VIDEO의 경우 전체 프레임 수)
            m_nTotalFrame = aviStreamInfo.dwLength;

            // 초당 샘플 수 (VIDEO의 경우 초당 프레임 수)
            m_nFrameRate = aviStreamInfo.dwRate / aviStreamInfo.dwScale;

            // 비디오 화면의 가로, 세로 크기
            RECT rc = aviStreamInfo.rcFrame;
            m_nWidth = rc.right - rc.left;
            m_nHeight = rc.bottom - rc.top;

            // 비디오 스트림으로부터 압축되지 않은 비디오 프레임을 준비한다.
            m_pVideoFrame = AVIStreamGetFrameOpen(m_pVideoStream, nullptr);

            if (!m_pVideoFrame)
            {
                Close();
                return FALSE;
            }
        }
        else
        {
            // Video 스트림이 아닌 경우 사용하지 않고 그대로 반환
            AVIStreamRelease(pAviTmpStream);
        }
    }
    return TRUE;
}

void IppAvi::Close()
{
    if (m_pVideoFrame != nullptr)
    {
        AVIStreamGetFrameClose(m_pVideoFrame);
        m_pVideoFrame = nullptr;
    }

    if (m_pVideoStream != nullptr)
    {
        AVIStreamRelease(m_pVideoStream);
        m_pVideoStream = nullptr;
    }

    if (m_pAviFile != nullptr)
    {
        AVIFileRelease(m_pAviFile);
        m_pAviFile = nullptr;
    }
}

void IppAvi::DrawFrame(HDC hDC, int nFrame)
{
    BYTE* pDib = (BYTE*)AVIStreamGetFrame(m_pVideoFrame, nFrame);
    if (!pDib)
        return;

    LPBITMAPINFO lpbi;
    LPSTR lpDIBBits;

    lpbi = (LPBITMAPINFO)pDib;

    if (lpbi->bmiHeader.biBitCount == 8)
        lpDIBBits = (LPSTR)pDib + sizeof(BITMAPINFOHEADER) + 1024;
    else
        lpDIBBits = (LPSTR)pDib + sizeof(BITMAPINFOHEADER);

    ::SetDIBitsToDevice(hDC,    // hDC
        0,                      // Dest X
        0,                      // Dest Y
        m_nWidth,               // nSrcWidth
        m_nHeight,              // nSrcHeight
        0,                      // Src X
        0,                      // Src Y
        0,                      // nStartScan
        (WORD)m_nHeight,        // nNumScans
        lpDIBBits,              // lpBits
        lpbi,                   // lpBitsInfo
        DIB_RGB_COLORS);        // wUsage
}

void IppAvi::DrawFrame(HDC hDC, int nFrame, int dx, int dy)
{
    DrawFrame(hDC, nFrame, dx, dy, GetWidth(), GetHeight());
}

void IppAvi::DrawFrame(HDC hDC, int nFrame, int dx, int dy, int dw, int dh, DWORD dwRop)
{
    BYTE* pDib = (BYTE*)AVIStreamGetFrame(m_pVideoFrame, nFrame);
    if (!pDib)
        return;

    LPBITMAPINFO lpbi;
    LPSTR lpDIBBits;
    lpbi = (LPBITMAPINFO)pDib;

    if (lpbi->bmiHeader.biBitCount == 8)
        lpDIBBits = (LPSTR)pDib + sizeof(BITMAPINFOHEADER) + 1024;
    else
        lpDIBBits = (LPSTR)pDib + sizeof(BITMAPINFOHEADER);

    ::SetStretchBltMode(hDC, COLORONCOLOR);
    ::StretchDIBits(hDC,               // hDC
        dx,                            // Dest X
        dy,                            // Dest Y
        dw,                            // nDestWidth
        dh,                            // nDestHeight
        0,                             // Src X
        0,                             // Src Y
        m_nWidth,                      // nStartScan
        m_nHeight,                     // nNumScans
        lpDIBBits,                     // lpBits
        lpbi,                          // lpBitsInfo    
        DIB_RGB_COLORS,                // wUsage
        SRCCOPY);                      // dwROP
}

BOOL IppAvi::GetFrame(int nFrame, IppDib & dib)
{
    LPVOID lpDib;
    LPSTR lpDIBBits;
    LPBITMAPINFO lpbi;

    lpDib = AVIStreamGetFrame(m_pVideoFrame, nFrame);
    lpbi = (LPBITMAPINFO)lpDib;

    if (lpbi->bmiHeader.biBitCount == 8)
        lpDIBBits = (LPSTR)lpDib + sizeof(BITMAPINFOHEADER) + 1024;
    else
        lpDIBBits = (LPSTR)lpDib + sizeof(BITMAPINFOHEADER);

    if (lpbi->bmiHeader.biBitCount == 8)
    {
        lpDIBBits = (LPSTR)lpDib + sizeof(BITMAPINFOHEADER) + 1024;

        dib.CreateGrayBitmap(m_nWidth, m_nHeight);
        memcpy(dib.GetBitmapInfoAddr(), lpDib, lpbi->bmiHeader.biSize + 1024 + 
            lpbi->bmiHeader.biSizeImage);
    }
    else if(lpbi->bmiHeader.biBitCount == 24)
    {
        lpDIBBits = (LPSTR)lpDib + sizeof(BITMAPINFOHEADER);

        dib.CreateRGBBitmap(m_nWidth, m_nHeight);
        memcpy(dib.GetBitmapInfoAddr(), lpDib, lpbi->bmiHeader.biSize 
            + lpbi->bmiHeader.biSizeImage);
    }
    else
    {
        return FALSE;
    }

    return TRUE;
}
