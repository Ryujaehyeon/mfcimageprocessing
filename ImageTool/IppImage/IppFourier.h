#pragma once

#include "IppImage.h"

class IppFourier
{
public:
    IppFourier();
    virtual ~IppFourier();

public:
    int width;      // 실수부
    int height;     // 허수부
    IppDoubleImage real;
    IppDoubleImage imag;

    void SetImage(IppByteImage& img);
    void GetImage(IppByteImage& img);
    void GetSpectrumImage(IppByteImage& img);
    void GetPhaseImage(IppByteImage& img);

    // 영상의 푸리에 변환
    void DFT(int dir);
    void DFTRC(int dir);
    void FFT(int dir);

    // 주파수 공간에서의 필터링 함수
    void LowPassIdeal(int cutoff);
    void HighPassIdeal(int cutoff);
    void LowPassGaussian(int cutoff);
    void HighPassGaussian(int cutoff);
};

void DFT1d(double* re, double* im, int N, int dir);
void FFT1d(double* re, double* im, int N, int dir);
bool IsPowerOf2(int n);
