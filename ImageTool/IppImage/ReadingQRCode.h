#pragma once
#include "IppImage.h"

template<typename T>
auto Median(T &t);
void QRCodeCorrection(IppByteImage& imgSrc, IppByteImage& imgDst);
