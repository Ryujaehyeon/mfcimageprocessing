#include "stdafx.h"
#include "ReadingQRCode.h"

#include <vector>
#include <algorithm>
#include <functional>
#include <numeric>

template<typename T>
auto Median(T &t)
{
    int nSize = t.size();
    if (nSize % 2 == 1)
        return t.at((nSize + 1) / 2);
    else
        return (t.at(nSize / 2) + t.at(nSize / 2 + 1)) / 2;
}

void QRCodeCorrection(IppByteImage & imgSrc, IppByteImage & imgDst)
{
    int w = imgSrc.GetWidth();
    int h = imgSrc.GetHeight();

    //imgDst = imgSrc;
    imgDst.CreateImage(w,h);

    BYTE** pSrc = imgSrc.GetPixels2D();
    BYTE** pDst = imgDst.GetPixels2D();

    std::vector<double> vAvgWidth, vAvgHeight;
    double dSumW = 0.0, dSumH = 0.0;
    double dAvgW = 0.0, dAvgH = 0.0;
    double dAvgWH = 0.0;
    int i, j;

    // 이미지 색상 평균을 구함
    // 모든 픽셀의 색상값 평균
    for (j = 0; j < h; j++)
    {
        for (i = 0; i < w; i++)
        {
            if (j == 0 && i == 0)
            {
                pDst[j][i] = 255;
            }
            dAvgWH += pSrc[j][i];
        }
    }
    dAvgWH /= (w*h);

    // 가로 픽셀들의 색상 평균
    for (j = 0; j < h; j++)
    {
        dSumW = 0;
        for (i = 0; i < w; i++)
            dSumW += pSrc[j][i];

        vAvgHeight.push_back(dSumW / w);//가로 픽셀 평균
    }

    // 세로 픽셀들의 색상 평균
    for (j = 0; j < w; j++)
    {
        dSumH = 0;
        for (i = 0; i < h; i++)
            dSumH += pSrc[i][j];

        vAvgWidth.push_back(dSumH / h);//세로 픽셀 평균
    }

    // width 전체 평균
    dAvgW = accumulate(vAvgWidth.begin(), vAvgWidth.end(), 0.0) / vAvgWidth.size() + 0.5;

    // height 전체 평균
    dAvgH = accumulate(vAvgHeight.begin(), vAvgHeight.end(), 0.0) / vAvgHeight.size() + 0.5;

    int nWSize = vAvgWidth.size() - 1.0;
    int nHSize = vAvgHeight.size() - 1.0;

    CRect rtQRSize = { 0,0,0,0 };

    // 첫 블럭 마킹의 위치를 색상 정보를 토대로 찾음
    // rt leftX
    for (double leftX = 0; leftX < vAvgWidth.size(); leftX++)
    {
        if (vAvgWidth.at(static_cast<int>(leftX)) > dAvgW)
        {
            rtQRSize.left = static_cast<LONG>(leftX-1);
            break;
        }
    }

    // rt rightX
    for (double rightX = vAvgWidth.size(); rightX >= 0.0; rightX--)
    {
        if (vAvgWidth.at(static_cast<int>(rightX - 1.0)) > dAvgW)
        {
            rtQRSize.right = static_cast<LONG>(rightX);
            break;
        }
    }

    // rt topY
    for (double topY = 0; topY < vAvgHeight.size(); topY++)
    {
        if (vAvgHeight.at(static_cast<int>(topY)) > dAvgH)
        {
            rtQRSize.top = static_cast<LONG>(topY-1);
            break;
        } 
    }

    // rt bottomY
    for (double bottomY = vAvgHeight.size(); bottomY >= 0.0; bottomY--)
    {
        if (vAvgHeight.at(static_cast<int>(bottomY - 1.0)) > dAvgH)
        {
            rtQRSize.bottom = static_cast<LONG>(bottomY);
            break;
        }
    }

    // 블럭 보정 작업 
    // 블럭 개수
    constexpr double nBlockCnt = 16.0;
    double dBlockX = (rtQRSize.Width() / nBlockCnt) ;
    double dBlockY = (rtQRSize.Height() / nBlockCnt);

    long lPixelY = 0, lPixelX = 0;
    int nPixelCnt = 0;
    double dPixelAvg = 0;
    constexpr double offset = 0.5;

    // QR CODE내 블럭 좌표
    for (double nblkHeight = 0; nblkHeight < nBlockCnt; nblkHeight++) {
        for (double nblkWidth = 0; nblkWidth < nBlockCnt; nblkWidth++)
        {
            //if (nblkHeight != 4 || nblkWidth != 15)
            //    continue;
            BYTE bGrayValue = 0;

            dPixelAvg = 0;
            nPixelCnt = 0;
            // 원본 이미지에서 블럭단위 픽셀 합계 및 평균
            for (double nPixelY = 0; nPixelY < dBlockY; nPixelY++) {
                for (double nPixelX = 0; nPixelX < dBlockX; nPixelX++)
                {   // 2차원 좌표 
                    // 이미지[찍을 위치Y + (블럭 좌표Y * 블럭 사이즈Y) + 픽셀위치][찍을 위치X + (블럭 좌표X * 블럭 사이즈X) + 픽셀위치]

                    // 1차원 좌표 
                    // 이미지[((찍을 위치Y + (블럭 세로(Y) 좌표 * 블럭 사이즈Y) + 픽셀위치Y) * 가로폭)
                    //       + (찍을 위치X + (블럭 가로(X) 좌표 * 블럭 사이즈X) + 픽셀위치X)

                    // 소수값으로 인한 오차는 0.5를 더해 보정
                    lPixelY = static_cast<int>(rtQRSize.top + (nblkHeight * dBlockY + offset) + nPixelY);
                    lPixelX = static_cast<int>(rtQRSize.left + (nblkWidth * dBlockX + offset) + nPixelX);

                    dPixelAvg += pSrc[lPixelY][lPixelX];
                    nPixelCnt++;
                }
            }

            //블럭 색상 평균
            dPixelAvg /= nPixelCnt;

            // 색상 값 
            // 이미지 그레이레벨보다 블럭 그레이 레벨이
            // 높으면 흰 블럭
            // 낮으면 검은 블럭
            if (dAvgWH > dPixelAvg)
                bGrayValue = 255;
            else
                bGrayValue = 0;

            // 대상 이미지의 픽셀 채우기
            for (double nPixelY = 0; nPixelY < dBlockY; nPixelY++) {
                for (double nPixelX = 0; nPixelX < dBlockX; nPixelX++)
                {
                    lPixelY = static_cast<int>(rtQRSize.top + (nblkHeight * dBlockY + offset) + nPixelY);
                    lPixelX = static_cast<int>(rtQRSize.left + (nblkWidth * dBlockX + offset) + nPixelX);
                    pDst[lPixelY][lPixelX] = ~bGrayValue; // QR Code 블럭은 검은색이기 때문에 블럭내 픽셀 색상을 반전시킴
                }
            }
        }
    }
}